let studentJan = [];

studentJan["name"] = "jan";
studentJan["surname"] = "reno"
studentJan["age"] = "26";

studentJan["score"] = [];
studentJan["score"]["javascript"] = 62;
studentJan["score"]["react"] = 57;
studentJan["score"]["python"] = 88;
studentJan["score"]["java"] = 90;

studentJan["result"] = [];
studentJan["result"]["sumPoints"] = studentJan["score"]["javascript"] + studentJan["score"]["react"] + studentJan["score"]["python"] + studentJan["score"]["java"];
studentJan["result"]["avePoints"] = (studentJan["result"]["sumPoints"]) / 4;

let janFrontAve = (studentJan["score"]["javascript"] + studentJan["score"]["react"]) / 2;

let janGpa = 0;

if (studentJan["score"]["javascript"] >= 51 && studentJan["score"]["javascript"] <= 60 ) {
    janGpa += 0.5 * 4;
} else if(studentJan["score"]["javascript"] >= 61 && studentJan["score"]["javascript"] <= 70) {
    janGpa += 1 * 4;
} else if(studentJan["score"]["javascript"] >= 71 && studentJan["score"]["javascript"] <= 80) {
    janGpa += 2 * 4;
} else if(studentJan["score"]["javascript"] >= 81 && studentJan["score"]["javascript"] <= 90) {
    janGpa += 3 * 4;
} else if(studentJan["score"]["javascript"] >= 91 && studentJan["score"]["javascript"] <= 100) {
    janGpa += 4 * 4;
}

if (studentJan["score"]["react"] >= 51 && studentJan["score"]["react"] <= 60 ) {
    janGpa += 0.5 * 7;
} else if(studentJan["score"]["react"] >= 61 && studentJan["score"]["react"] <= 70) {
    janGpa += 1 * 7;
} else if(studentJan["score"]["react"] >= 71 && studentJan["score"]["react"] <= 80) {
    janGpa += 2 * 7;
} else if(studentJan["score"]["react"] >= 81 && studentJan["score"]["react"] <= 90) {
    janGpa += 3 * 7;
} else if(studentJan["score"]["react"] >= 91 && studentJan["score"]["react"] <= 100) {
    janGpa += 4 * 7;
}

if (studentJan["score"]["python"] >= 51 && studentJan["score"]["python"] <= 60 ) {
    janGpa += 0.5 * 6;
} else if(studentJan["score"]["python"] >= 61 && studentJan["score"]["python"] <= 70) {
    janGpa += 1 * 6;
} else if(studentJan["score"]["python"] >= 71 && studentJan["score"]["python"] <= 80) {
    janGpa += 2 * 6;
} else if(studentJan["score"]["python"] >= 81 && studentJan["score"]["python"] <= 90) {
    janGpa += 3 * 6;
} else if(studentJan["score"]["python"] >= 91 && studentJan["score"]["python"] <= 100) {
    janGpa += 4 * 6;
}

if (studentJan["score"]["java"] >= 51 && studentJan["score"]["java"] <= 60 ) {
    janGpa += 0.5 * 3;
} else if(studentJan["score"]["java"] >= 61 && studentJan["score"]["java"] <= 70) {
    janGpa += 1 * 3;
} else if(studentJan["score"]["java"] >= 71 && studentJan["score"]["java"] <= 80) {
    janGpa += 2 * 3;
} else if(studentJan["score"]["java"] >= 81 && studentJan["score"]["java"] <= 90) {
    janGpa += 3 * 3;
} else if(studentJan["score"]["java"] >= 91 && studentJan["score"]["java"] <= 100) {
    janGpa += 4 * 3;
}

janGpa /= 20;

console.log(studentJan);
console.log(janGpa + " " + "GPA");

studentClode = [];
studentClode["name"] = "clode";
studentClode["surname"] = "mone"
studentClode["age"] = "19";

studentClode["score"] = [];
studentClode["score"]["javascript"] = 77;
studentClode["score"]["react"] = 52;
studentClode["score"]["python"] = 92;
studentClode["score"]["java"] = 67;

studentClode["result"] = [];
studentClode["result"]["sumPoints"] = studentClode["score"]["javascript"] + studentClode["score"]["react"] + studentClode["score"]["python"] + studentClode["score"]["java"];
studentClode["result"]["avePoints"] = (studentClode["result"]["sumPoints"]) / 4;

let clodeFrontAve = (studentClode["score"]["javascript"] + studentClode["score"]["react"]) / 2;

let clodeGpa = 0;

if (studentClode["score"]["javascript"] >= 51 && studentClode["score"]["javascript"] <= 60 ) {
    clodeGpa += 0.5 * 4;
} else if(studentClode["score"]["javascript"] >= 61 && studentClode["score"]["javascript"] <= 70) {
    clodeGpa += 1 * 4;
} else if(studentClode["score"]["javascript"] >= 71 && studentClode["score"]["javascript"] <= 80) {
    clodeGpa += 2 * 4;
} else if(studentClode["score"]["javascript"] >= 81 && studentClode["score"]["javascript"] <= 90) {
    clodeGpa += 3 * 4;
} else if(studentClode["score"]["javascript"] >= 91 && studentClode["score"]["javascript"] <= 100) {
    clodeGpa += 4 * 4;
}

if (studentClode["score"]["react"] >= 51 && studentClode["score"]["react"] <= 60 ) {
    clodeGpa += 0.5 * 7;
} else if(studentClode["score"]["react"] >= 61 && studentClode["score"]["react"] <= 70) {
    clodeGpa += 1 * 7;
} else if(studentClode["score"]["react"] >= 71 && studentClode["score"]["react"] <= 80) {
    clodeGpa += 2 * 7;
} else if(studentClode["score"]["react"] >= 81 && studentClode["score"]["react"] <= 90) {
    clodeGpa += 3 * 7;
} else if(studentClode["score"]["react"] >= 91 && studentClode["score"]["react"] <= 100) {
    clodeGpa += 4 * 7;
}

if (studentClode["score"]["python"] >= 51 && studentClode["score"]["python"] <= 60 ) {
    clodeGpa += 0.5 * 6;
} else if(studentClode["score"]["python"] >= 61 && studentClode["score"]["python"] <= 70) {
    clodeGpa += 1 * 6;
} else if(studentClode["score"]["python"] >= 71 && studentClode["score"]["python"] <= 80) {
    clodeGpa += 2 * 6;
} else if(studentClode["score"]["python"] >= 81 && studentClode["score"]["python"] <= 90) {
    clodeGpa += 3 * 6;
} else if(studentClode["score"]["python"] >= 91 && studentClode["score"]["python"] <= 100) {
    clodeGpa += 4 * 6;
}

if (studentClode["score"]["java"] >= 51 && studentClode["score"]["java"] <= 60 ) {
    clodeGpa += 0.5 * 3;
} else if(studentClode["score"]["java"] >= 61 && studentClode["score"]["java"] <= 70) {
    clodeGpa += 1 * 3;
} else if(studentClode["score"]["java"] >= 71 && studentClode["score"]["java"] <= 80) {
    clodeGpa += 2 * 3;
} else if(studentClode["score"]["java"] >= 81 && studentClode["score"]["java"] <= 90) {
    clodeGpa += 3 * 3;
} else if(studentClode["score"]["java"] >= 91 && studentClode["score"]["java"] <= 100) {
    clodeGpa += 4 * 3;
}

clodeGpa /= 20;

console.log(studentClode);
console.log(clodeGpa + " " + "GPA");

studentVan = [];
studentVan["name"] = "van";
studentVan["surname"] = "gogh"
studentVan["age"] = "21";

studentVan["score"] = [];
studentVan["score"]["javascript"] = 51;
studentVan["score"]["react"] = 98;
studentVan["score"]["python"] = 65;
studentVan["score"]["java"] = 70;

studentVan["result"] = [];
studentVan["result"]["sumPoints"] = studentVan["score"]["javascript"] + studentVan["score"]["react"] + studentVan["score"]["python"] + studentVan["score"]["java"];
studentVan["result"]["avePoints"] = (studentVan["result"]["sumPoints"]) / 4;

let vanFrontAve = (studentVan["score"]["javascript"] + studentVan["score"]["react"]) / 2;

let vanGpa = 0;

if (studentVan["score"]["javascript"] >= 51 && studentVan["score"]["javascript"] <= 60 ) {
    vanGpa += 0.5 * 4;
} else if(studentVan["score"]["javascript"] >= 61 && studentVan["score"]["javascript"] <= 70) {
    vanGpa += 1 * 4;
} else if(studentVan["score"]["javascript"] >= 71 && studentVan["score"]["javascript"] <= 80) {
    vanGpa += 2 * 4;
} else if(studentVan["score"]["javascript"] >= 81 && studentVan["score"]["javascript"] <= 90) {
    vanGpa += 3 * 4;
} else if(studentVan["score"]["javascript"] >= 91 && studentVan["score"]["javascript"] <= 100) {
    vanGpa += 4 * 4;
}

if (studentVan["score"]["react"] >= 51 && studentVan["score"]["react"] <= 60 ) {
    vanGpa += 0.5 * 7;
} else if(studentVan["score"]["react"] >= 61 && studentVan["score"]["react"] <= 70) {
    vanGpa += 1 * 7;
} else if(studentVan["score"]["react"] >= 71 && studentVan["score"]["react"] <= 80) {
    vanGpa += 2 * 7;
} else if(studentVan["score"]["react"] >= 81 && studentVan["score"]["react"] <= 90) {
    vanGpa += 3 * 7;
} else if(studentVan["score"]["react"] >= 91 && studentVan["score"]["react"] <= 100) {
    vanGpa += 4 * 7;
}

if (studentVan["score"]["python"] >= 51 && studentVan["score"]["python"] <= 60 ) {
    vanGpa += 0.5 * 6;
} else if(studentVan["score"]["python"] >= 61 && studentVan["score"]["python"] <= 70) {
    vanGpa += 1 * 6;
} else if(studentVan["score"]["python"] >= 71 && studentVan["score"]["python"] <= 80) {
    vanGpa += 2 * 6;
} else if(studentVan["score"]["python"] >= 81 && studentVan["score"]["python"] <= 90) {
    vanGpa += 3 * 6;
} else if(studentVan["score"]["python"] >= 91 && studentVan["score"]["python"] <= 100) {
    vanGpa += 4 * 6;
}

if (studentVan["score"]["java"] >= 51 && studentVan["score"]["java"] <= 60 ) {
    vanGpa += 0.5 * 3;
} else if(studentVan["score"]["java"] >= 61 && studentVan["score"]["java"] <= 70) {
    vanGpa += 1 * 3;
} else if(studentVan["score"]["java"] >= 71 && studentVan["score"]["java"] <= 80) {
    vanGpa += 2 * 3;
} else if(studentVan["score"]["java"] >= 81 && studentVan["score"]["java"] <= 90) {
    vanGpa += 3 * 3;
} else if(studentVan["score"]["java"] >= 91 && studentVan["score"]["java"] <= 100) {
    vanGpa += 4 * 3;
}

vanGpa /= 20;

console.log(studentVan);
console.log(vanGpa + " " + "GPA");

studentDamm = [];
studentDamm["name"] = "damm";
studentDamm["surname"] = "square"
studentDamm["age"] = "36";

studentDamm["score"] = [];
studentDamm["score"]["javascript"] = 82;
studentDamm["score"]["react"] = 53;
studentDamm["score"]["python"] = 80;
studentDamm["score"]["java"] = 65;

studentDamm["result"] = [];
studentDamm["result"]["sumPoints"] = studentDamm["score"]["javascript"] + studentDamm["score"]["react"] + studentDamm["score"]["python"] + studentDamm["score"]["java"];
studentDamm["result"]["avePoints"] = (studentDamm["result"]["sumPoints"]) / 4;

let dammFrontAve = (studentDamm["score"]["javascript"] + studentDamm["score"]["react"]) / 2;

let dammGpa = 0;

if (studentDamm["score"]["javascript"] >= 51 && studentDamm["score"]["javascript"] <= 60 ) {
    dammGpa += 0.5 * 4;
} else if(studentDamm["score"]["javascript"] >= 61 && studentDamm["score"]["javascript"] <= 70) {
    dammGpa += 1 * 4;
} else if(studentDamm["score"]["javascript"] >= 71 && studentDamm["score"]["javascript"] <= 80) {
    dammGpa += 2 * 4;
} else if(studentDamm["score"]["javascript"] >= 81 && studentDamm["score"]["javascript"] <= 90) {
    dammGpa += 3 * 4;
} else if(studentDamm["score"]["javascript"] >= 91 && studentDamm["score"]["javascript"] <= 100) {
    dammGpa += 4 * 4;
}

if (studentDamm["score"]["react"] >= 51 && studentDamm["score"]["react"] <= 60 ) {
    dammGpa += 0.5 * 7;
} else if(studentDamm["score"]["react"] >= 61 && studentDamm["score"]["react"] <= 70) {
    dammGpa += 1 * 7;
} else if(studentDamm["score"]["react"] >= 71 && studentDamm["score"]["react"] <= 80) {
    dammGpa += 2 * 7;
} else if(studentDamm["score"]["react"] >= 81 && studentDamm["score"]["react"] <= 90) {
    dammGpa += 3 * 7;
} else if(studentDamm["score"]["react"] >= 91 && studentDamm["score"]["react"] <= 100) {
    dammGpa += 4 * 7;
}

if (studentDamm["score"]["python"] >= 51 && studentDamm["score"]["python"] <= 60 ) {
    dammGpa += 0.5 * 6;
} else if(studentDamm["score"]["python"] >= 61 && studentDamm["score"]["python"] <= 70) {
    dammGpa += 1 * 6;
} else if(studentDamm["score"]["python"] >= 71 && studentDamm["score"]["python"] <= 80) {
    dammGpa += 2 * 6;
} else if(studentDamm["score"]["python"] >= 81 && studentDamm["score"]["python"] <= 90) {
    dammGpa += 3 * 6;
} else if(studentDamm["score"]["python"] >= 91 && studentDamm["score"]["python"] <= 100) {
    dammGpa += 4 * 6;
}


if (studentDamm["score"]["java"] >= 51 && studentDamm["score"]["java"] <= 60 ) {
    dammGpa += 0.5 * 3;
} else if(studentDamm["score"]["java"] >= 61 && studentDamm["score"]["java"] <= 70) {
    dammGpa += 1 * 3;
} else if(studentDamm["score"]["java"] >= 71 && studentDamm["score"]["java"] <= 80) {
    dammGpa += 2 * 3;
} else if(studentDamm["score"]["java"] >= 81 && studentDamm["score"]["java"] <= 90) {
    dammGpa += 3 * 3;
} else if(studentDamm["score"]["java"] >= 91 && studentDamm["score"]["java"] <= 100) {
    dammGpa += 4 * 3;
}

dammGpa /= 20;

console.log(studentDamm);
console.log(dammGpa + " " + "GPA");


//HIGHEST GPA

if(janGpa > clodeGpa && janGpa > vanGpa && janGpa > dammGpa) {
    console.log("Jan has the highest GPA" + " " + janGpa);
} else if(clodeGpa > janGpa && clodeGpa > vanGpa && clodeGpa > dammGpa) {
    console.log("Clode has the highest GPA" + " " + clodeGpa);
} else if(vanGpa > janGpa && vanGpa > clodeGpa && vanGpa > dammGpa) {
    console.log("Van has the highest GPA" + " " + vanGpa);
} else if(dammGpa > janGpa && dammGpa > clodeGpa && dammGpa > vanGpa) {
    console.log("Damm has the highest GPA" + " " + dammGpa)
}


// BEST STUDENT + 21

if(studentJan["age"] > 21 && 
    studentJan["result"]["avePoints"] > studentVan["result"]["avePoints"] && 
    studentJan["result"]["avePoints"] > studentClode["result"]["avePoints"] && 
    studentJan["result"]["avePoints"] > studentDamm["result"]["avePoints"]) {
        console.log("Jan is the best average student" + " " + studentJan["result"]["avePoints"]);
} else if(studentClode["age"] > 21 &&
    studentClode["result"]["avePoints"] > studentJan["result"]["avePoints"] &&
    studentClode["result"]["avePoints"] > studentVan["result"]["avePoints"] && 
    studentClode["result"]["avePoints"] > studentDamm["result"]["avePoints"]) {
        console.log("Clode is the best average student" + " " + studentClode["result"]["avePoints"]);
} else if(studentVan["age"] > 21 &&
    studentVan["result"]["avePoints"] > studentJan["result"]["avePoints"] &&
    studentVan["result"]["avePoints"] > studentClode["result"]["avePoints"] &&
    studentVan["result"]["avePoints"] > studentDamm["result"]["avePoints"]) {
        console.log("Van is the best average student" + " " + studentVan["result"]["avePoints"]);
} else if(studentDamm["age"] > 21 &&
    studentDamm["result"]["avePoints"] > studentJan["result"]["avePoints"] &&
    studentDamm["result"]["avePoints"] > studentVan["result"]["avePoints"] &&
    studentDamm["result"]["avePoints"] > studentClode["result"]["avePoints"]) {
        console.log("Damm is the best average student" + " " + studentDamm["result"]["avePoints"]);
}


// BEST FRONT-END STUDENT 

if(janFrontAve > clodeFrontAve &&
   janFrontAve > vanFrontAve &&
   janFrontAve > dammFrontAve) {
       console.log("Jan is the best front-end student" + " " + janFrontAve);
} else if(clodeFrontAve > janFrontAve &&
    clodeFrontAve > vanFrontAve &&
    clodeFrontAve > dammFrontAve) {
        console.log("Clode is the best front-end student" + " " + clodeFrontAve);
} else if(vanFrontAve > janFrontAve &&
    vanFrontAve > clodeFrontAve &&
    vanFrontAve > dammFrontAve) {
        console.log("Van is the best front-end student" + " " + vanFrontAve);
} else if(dammFrontAve > janFrontAve &&
    dammFrontAve > vanFrontAve &&
    dammFrontAve > clodeFrontAve) {
        console.log("Damm is the best front-end student" + " " + dammFrontAve);
}

